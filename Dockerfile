FROM python:3.6.12-alpine

RUN apk add \
	curl

WORKDIR /app

RUN curl https://raw.githubusercontent.com/getupcloud/python-mailgunlog/master/requirements.txt -o requirements.txt && \
	pip install -r requirements.txt

RUN pip install mailgunlog

ENTRYPOINT ["mailgunlog"]
