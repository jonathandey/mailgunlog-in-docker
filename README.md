# mailgunlog Dockerised

An implementation of https://github.com/getupcloud/python-mailgunlog for Docker.

Build & Run:
```
docker build . -t jonathandey/mailgunlog
docker run --rm jonathandey/mailgunlog DOMAIN_NAME MAILGUN_API_KEY --days 1
```